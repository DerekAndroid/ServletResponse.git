package com.itheima.d_out;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class OutServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//字符流和字节流不能同时出现   俩流互斥
		/*ServletOutputStream outputStream = response.getOutputStream();*/
		//解决响应乱码第一种方式
		//response.setHeader("content-type", "text/html;charset=utf-8");
		//解决响应乱码第二种方式
		response.setContentType("text/html;charset=utf-8");
		
		
		response.getWriter().print("<table border=1px>");
		
		response.getWriter().print("<tr>");
		
		response.getWriter().print("<td>");
		response.getWriter().print("汤姆猫");
		response.getWriter().print("</td>");
		
		response.getWriter().print("<td>");
		response.getWriter().print("123");
		response.getWriter().print("</td>");
		
		response.getWriter().print("</tr>");
		
		response.getWriter().print("</table>");
		
		
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
