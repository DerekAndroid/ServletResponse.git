package com.itheima.c_loc;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class MeServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		System.out.println("班长曰：浩哥，借我50");
		System.out.println("我说：没有");
		System.out.println("我说：班主任有");
		
		//重定向
		//第一种方式
		//设置重定向的状态码   302
//		response.setStatus(302);
//		//设置重定向的头  location
//		response.setHeader("location", "/ServletLogin/bzr");
		
		//第二种方式
		response.sendRedirect("/ServletLogin/bzr");
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
