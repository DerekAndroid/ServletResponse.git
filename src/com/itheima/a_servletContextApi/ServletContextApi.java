package com.itheima.a_servletContextApi;

import java.io.IOException;
import java.io.InputStream;
import java.util.Enumeration;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class ServletContextApi extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//获取ServletContext对象
		ServletContext sc = getServletContext();
		//获取全局初始化参数
		String db = sc.getInitParameter("db");
		System.out.println(db);
		//获取所有全局初始化参数名称
		Enumeration<String> initParameterNames = sc.getInitParameterNames();
		while (initParameterNames.hasMoreElements()) {
			String string = (String) initParameterNames.nextElement();
			System.out.println(string+":"+sc.getInitParameter(string));
		}
		//获取一个资源在服务器上的真实路径
		String realPath = sc.getRealPath("/1.html");
		System.out.println(realPath);
		//以流的形式返回一个文件
		InputStream is = sc.getResourceAsStream("/1.html");
		System.out.println(is);
		//获取一个文件的mime类型(大类型/小类型   image/gif)
		String mimeType = sc.getMimeType("/1.jpg");
		System.out.println(mimeType);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
